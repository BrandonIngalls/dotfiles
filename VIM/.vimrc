" be iMproved, required
set nocompatible

" Source the plugins file which contains all the plugins for vundle
source ~/.vim/user/plugins.vim

" Normal mode mappings
source ~/.vim/user/nmaps.vim

" Colorscheme settings
source ~/.vim/user/color-settings.vim

" Source core settings
source ~/.vim/user/core-settings.vim

" Source functions file
source ~/.vim/user/functions.vim

" Python options
let python_highlight_all = 1

" Hide buffers
set hidden

" Use system clipboard
set clipboard=unnamedplus

" Enhance command-line completion
set wildmenu

" Allow cursor keys in insert mode
set esckeys

" Allow backspace in insert mode
set backspace=indent,eol,start

" Optimize for fast terminal connections
set ttyfast

" Add the g flag to search/replace by default
set gdefault

" Use UTF-8 without BOM
set encoding=utf-8 nobomb

" Change mapleader
let mapleader=","

" Don’t add empty newlines at the end of files
set binary
set noeol

" Respect modeline in files
set modeline
set modelines=4
set shiftwidth=4

" Tabs to Spaces
set expandtab

" Make tabs as wide as two spaces
set tabstop=4

" Show “invisible” characters
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list

" Automatic commands
if has("autocmd")
	" Enable file type detection
	filetype on
	" Treat .json files as .js
	autocmd BufNewFile,BufRead *.json setfiletype json syntax=javascript
	" Treat .md files as Markdown
	autocmd BufNewFile,BufRead *.md setlocal filetype=markdown
endif
